package com.garnier.biblio;



import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;
import android.view.View.OnClickListener;

import static android.widget.Toast.*;

public class Biblio extends Activity {
    EditText issnET;
    EditText titreET;
    EditText auteurET;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        TextView textView= new TextView(this);
        setContentView(R.layout.main);
       // setContentView(textView);
        issnET = (EditText) findViewById(R.id.editTextisbn);
        titreET = (EditText) findViewById(R.id.editTexttitre);
        auteurET = (EditText) findViewById(R.id.editTextauteur);
        //Création d'une instance de ma classe LivresBDD
        LivresBDD livreBdd = new LivresBDD(this);
 
        //Création d'un livre
        final Livre livre = new Livre("123456789", "Programmez pour Android", "lauteur");
        Livre livr = new Livre("000154", "poi", "michel");

        makeText(this, livr.toString(), LENGTH_LONG).show();

        //On ouvre la base de données pour écrire dedans
        livreBdd.open();
        //On insère le livre que l'on vient de créer
        livreBdd.insertLivre(livre);
        livreBdd.insertLivre(livr);


        //Pour vérifier que l'on a bien créé notre livre dans la BDD
        //on extrait le livre de la BDD grâce au titre du livre que l'on a créé précédemment
        Livre livreFromBdd = livreBdd.getLivreWithTitre(livre.getTitre());
        //Si un livre est retourné (donc si le livre à bien été ajouté à la BDD)
        if(livreFromBdd != null){
        	//On affiche les infos du livre dans un Toast
        	makeText(this, livreFromBdd.toString(), LENGTH_LONG).show();
        	//On modifie le titre du livre
        	livreFromBdd.setTitre("J'ai modifié le titre du livre");
        	//Puis on met à jour la BDD
            livreBdd.updateLivre(livreFromBdd.getId(), livreFromBdd);
        }
 
        //On extrait le livre de la BDD grâce au nouveau titre
        livreFromBdd = livreBdd.getLivreWithTitre("J'ai modifié le titre du livre");
        //S'il existe un livre possédant ce titre dans la BDD
        if(livreFromBdd != null){
	        //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour
	        makeText(this, livreFromBdd.toString(), LENGTH_LONG).show();
	        //on supprime le livre de la BDD grâce à son ID
	    	livreBdd.removeLivreWithID(livreFromBdd.getId());
        }
        //On essait d'extraire de nouveau le livre de la BDD toujours grâce à son nouveau titre
        livreFromBdd = livreBdd.getLivreWithTitre("J'ai modifié le titre du livre");
        //Si aucun livre n'est retourné
        if(livreFromBdd == null){
        	//On affiche un message indiquant que le livre n'existe pas dans la BDD
        	makeText(this, "Ce livre n'existe pas dans la BDD", LENGTH_LONG).show();
        }
        //Si le livre existe (mais normalement il ne devrait pas)
        else{
        	//on affiche un message indiquant que le livre existe dans la BDD
        	makeText(this, "Ce livre existe dans la BDD", LENGTH_LONG).show();
        }

        /*Connection DB*/
        LivresBDD db = new LivresBDD(getApplicationContext());
        db.open();
        Livre li = livreBdd.getLivreWithTitre("poi");
        makeText(this, li.getTitre(), LENGTH_LONG).show();
        livreBdd.close();

        //On écrit sur dans les champs
        remplirChamp(li);
        //Gestion des boutons
        Button next = (Button) findViewById(R.id.boutnext);
        Button prev = (Button) findViewById(R.id.boutprev);
        Button modifier = (Button) findViewById(R.id.boutmodif);
        Button ajouter = (Button) findViewById(R.id.boutajouter);
        Button supp = (Button) findViewById(R.id.boutsupp);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  System.out.println("on a cliquer sur next !" + li.toString());
                LivresBDD db = new LivresBDD(getApplicationContext());
                db.open();
                //titre afficé
                String titre = titreET.getText().toString();
                //livre
                Livre liAffiche = db.getLivreWithTitre(titre);
                //id
                int id =liAffiche.getId();
                Livre nextLi = db.getLivreWithId(id);
                remplirChamp(nextLi);
            }
        });



    }
    private void remplirChamp(Livre li){
        issnET.setText(li.getIsbn());
        titreET.setText(li.getTitre());
        auteurET.setText(li.getAuteur());
    }
}