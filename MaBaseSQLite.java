package com.garnier.biblio;
 
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
 
public class MaBaseSQLite extends SQLiteOpenHelper {
	//le nom des tables
	private static final String TABLE_LIVRES = "compte";
	private static final String TABLE_BANQUE = "banque";
	private static final String TABLE_MODIF = "mouvement";
	//les colonnes
	private static final String COL_IDBANQUE = "idBanque";
	private static final String COL_NOMBANQUE = "nomBanque";

	
	private static final String COL_IDCOMPTE = "idCompte";
	private static final String COL_NOMCOMPTE = "nomCompte";
	private static final String COL_LIBELLECOMPTE = "libelleCompte";

	private static final String COL_IDMOUVEMENT = "idMouvement";
	private static final String COL_LIBELLEMOUVEMENT = "libelleMouvement";
	private static final String COL_DATEMOUVEMENT = "dateMouvement";
	private static final String COL_MONTANTMOUVEMENT = "montantMouvement";




	private static final String CREATE_BDD = "CREATE TABLE " + TABLE_LIVRES + " ("
	+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_ISBN + " TEXT NOT NULL, "
	+ COL_TITRE + " TEXT NOT NULL, "+ COL_AUTEUR+" TEXT );";
 
	public MaBaseSQLite(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {
		//on créé la table à partir de la requête écrite dans la variable CREATE_BDD

		db.execSQL(CREATE_BDD);
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//On peut fait ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
		//comme ça lorsque je change la version les id repartent de 0
		db.execSQL("DROP TABLE " + TABLE_LIVRES + ";");
		onCreate(db);
	}
 
}